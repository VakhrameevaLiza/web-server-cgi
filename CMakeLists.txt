cmake_minimum_required(VERSION 3.0)
project(Server)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp do_service.cpp do_service.h start_web_server.cpp start_web_server.h)
add_executable(Server ${SOURCE_FILES})