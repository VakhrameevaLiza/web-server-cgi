#include "start_web_server.h"

const char * DefaultPage = "/README.html";

int socket_descr;

void handler(int s) {
    close(socket_descr);

}

int fill_server_addr(struct sockaddr_in& server_addr, uint16_t port_number, const char* ip) {
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_number);
    if (inet_addr(ip) == -1) {
        return -1;
    }
    server_addr.sin_addr.s_addr = inet_addr(ip);
    return 0;
}

void update_env(std::string http_form) {
    size_t space_pos =  http_form.find(' ');
    std::string method = http_form.substr(0, space_pos);
    http_form = http_form.substr(space_pos + 1);
    space_pos = http_form.find(' ');
    std::string query = http_form.substr(0, space_pos);
    size_t args_begin = query.find('?');
    std::string args = "";
    if (args_begin != std::string::npos) {
        args = query.substr(args_begin + 1);
        const char slash = '/';
        if (http_form[args_begin - 1] == slash)
            query = query.substr(0, args_begin - 1);
        else
            query = query.substr(0, args_begin);
    }
    if (strcmp(query.c_str(), "/") == 0)
        query = DefaultPage;
    setenv("METHOD", method.c_str(), 1);
    setenv("QUERY_STRING", args.c_str(), 1);
    setenv("REQUEST_URI", query.c_str(), 1);
}

int start_web_server(uint16_t port_number, const char* ip) {


    signal(SIGKILL, handler);

    socket_descr = socket(AF_INET, SOCK_STREAM, INADDR_ANY);
    if (socket_descr < 0) {
        std::cerr << "socket() fail" << std::endl;
        return -1;
    }


    struct sockaddr_in server_addr;
    if (fill_server_addr(server_addr, port_number, ip) == -1) {
        std::cerr << "fill_server_addr fail" << std::endl;
        return -1;
    }
    uint32_t new_port_number = port_number;
    if (bind(socket_descr, (struct sockaddr *) &server_addr, sizeof(server_addr))) {

        if (errno == EADDRINUSE or errno == EACCES) {
            uint16_t i = 1024;
            for (; i < 65536; i++) {
                if ((fill_server_addr(server_addr, i, ip) == -1) == 0 &
                        bind(socket_descr, (struct sockaddr *) &server_addr, sizeof(server_addr)) == 0) {
                    new_port_number = i;
                    break;
                }
            }
            if (i == 65536) {
                std::cerr << "No free port" << std::endl;
                return -1;
            }
        } else {
            std::cerr << "bind() fail " << errno << std::endl;
            return -1;
        }
    }
    std::string s = std::to_string(new_port_number);
    setenv("SERVER_PORT", s.c_str(), 1);
    if (new_port_number != port_number)
     std::cout << "Port number has changed. New port number: " << new_port_number << std::endl;

    if (listen(socket_descr, 100)) {
        std::cerr << "listen() fail" << errno << std::endl;
        return -1;
    }

    while (true) {
        int in_socket_descr = accept(socket_descr, NULL, NULL);

        if (in_socket_descr == -1) {
            std::cerr << "accept() FAIL " << errno << std::endl;
            close(socket_descr);
            return -1;
        }

        char request[1000];
        memset(request, 0, 1000);
        if (read(in_socket_descr,request,255) < 0) {
            std::cerr << "read() fail" << errno << std::endl;
            close(in_socket_descr);
            close(socket_descr);
            return -1;
        }
        update_env(std::string(request));

        int pid = fork();
        if (pid == 0) {
            dup2(in_socket_descr, 1);
            do_service(in_socket_descr);
            exit(0);
        } else {
            wait(NULL);
        }
        close(in_socket_descr);
    }
    close(socket_descr);
    return 0;
}