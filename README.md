##Веб-сервер с функциональностью cgi
##Выполнила: Вахрамеева Елизавета, 145-1гр

**Веб-сервер** - это программа, установленная на физическом сервере (машине, которая предназначена для выполнения разнообразных задач без участия людей),
которая обеспечивает доступ по сети к данным, которые хранятся на этой машине.
К веб-серверу обращаются по уникальному адресу, который есть у данной машины. 
Сервер ждет обращения по данному адресу, а затем обрабатывает его и возвращает какой-то ответ. 
При этом запросы браузера серверу и ответы сервера браузеру формируются в соответсвии с протоколом http.
Важно отметить, что веб-сервер должен уметь обрабатывать множество запросов одновременно. 


**CGI (Common Gateway Interface)** - формат взаимодействия веб-сервера и других программ, выполняющихся на физическом сервере.
Использование cgi позволяет делать страницы динамическими, т.е. позволяет автоматически изменять ответ, отправляемый на запрос клиента,
в соответсвии с каким-то текущими условиями, которые каждый раз разные.
Например, чтобы иметь возможность для запроса mysite.com/name=some_name выводить на странице сайте "Hello, some_name", нужен cgi.
Т.е. cgi позволит нам на сервере завести программу, которая будет парсить строку name=some_name, узнавать из нее то,
что имя - это some_name, и соответсвенно ей изменять ответ, который веб-сервер пошлет обратно пользователю, представившемуся как some_name.


##Параметры программы



**Аргументы командой строки**


argv[0] (обязательный) - директория, в которой хранятся запрашиваемые файлы (папка files)


argv[1] (по умолчанию 80) - номер порта, если указанный порт занят, то сервер выбирает первый свободный, о чем оведомляет
                            сообщением на стандартном потоке вывода


argv[2] (по умолчанию 127.0.0.1) - имя хоста



**Переменные среды**, к которым имеют доступ и сервер, и запускаемый им сторонний скрипт (hostname:portno/cgi-env)
SERVER_PORT


HOST_NAME


STORAGE_DIRECTORY


REQUEST_URI


QUERY_STRING


**HTTP-заголовок**


В HTTP-заголовке сервер отправляет статус, дату, тип и размер ответа


##Реализация


4 балла. Реализован веб-сервер, который умеет выдавать статические страницы и обслуживать одновременно несколько подключений
Программа создает сокет, отвечающий заданному ip-адресу и номеру порта. Затем программа прослушивает данное соединение в ожидании обращений.
Как только сервер получает обращение, он создает для него отдельный файловый дескриптор и отправляет запрошенные данные, если файл с данным именем доступен для чтения.


6 баллов. Помимо выдачи статического содержимого, сервер умеет выполнять сторонние программы и выдавать результат их работы
Как только сервер получает обращение и создает для него файловый дескриптор, происходит перенаправление потоков с помощью dup2(со стандартного потока вывода
на созданный файловый дескриптор), затем создается дочерний процесс, который обслуживает запрос. Если запрос динамический, то с помощью execve запускается скрипт, 
который полностью обслуживает данное соединение (то есть сам составляет http header и ответ), который в коде скрипта выводится на стандартный поток вывода. В скрипт так же передаются оговоренные выше переменные окружения.
