#pragma once

#ifndef SERVER_START_WEB_SERVER_H
#define SERVER_START_WEB_SERVER_H

#include <stdint.h>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string>
#include <signal.h>

#include "do_service.h"


int fill_server_addr(struct sockaddr_in&, uint16_t, const char*);
int start_web_server(uint16_t, const char*);

#endif //SERVER_START_WEB_SERVER_H
