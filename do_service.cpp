#include "do_service.h"

enum ContentType {html, text, jpeg, jpg, png, not_found};

const size_t MaxHeaderSize = 1000;
const size_t MaxEnvironmentSize = 500;
const char HeaderEnd[] = "\r\nConnection: close\r\n\r\n";

void generate_header(char *header, ContentType type, bool not_found) {
    const char * status;
    if (not_found)
        status = "HTTP/1.1 404 Not Found\r\n";
    else
        status = "HTTP/1.1 200 OK\r\n";

    strcat(header, status);
    time_t rawtime;
    time(&rawtime);
    char * time = ctime(&rawtime);
    strcat(header, "Date: ");
    strcat(header, time);

    strcat(header, "Content-Type: ");
    switch (type) {
        case html:
            strcat(header, "text/html;charset=UTF-8\r\n");
        case text:
            strcat(header, "text;charset=UTF-8\r\n");
            break;
        case jpeg:
            strcat(header,"image/jpeg\r\n");
            break;
        case jpg:
            strcat(header, "image/jpg\r\n");
            break;
        case png:
            strcat(header, "image/png\r\n");
            break;
    }
    strcat(header, "Content-Length: ");
}

void serve_static_content(int fd, bool not_found) {
    char* storage_dir = getenv("STORAGE_DIRECTORY");
    char* path_to_add = getenv("REQUEST_URI");
    size_t size = strlen(storage_dir) + strlen(path_to_add);
    char full_path[size];
    memset(full_path, 0, size);
    strcat(full_path, storage_dir);
    strcat(full_path, path_to_add);
    int file_fd = open(full_path, O_RDONLY);
    size_t file_size = lseek(file_fd, 0, SEEK_END);
    const char * output_size = (std::to_string(file_size)).c_str();
    lseek(file_fd, 0, SEEK_SET);
    char* memory = (char *) mmap(NULL, file_size, PROT_READ, MAP_SHARED, file_fd, 0);
    ContentType type;
    if (strstr(path_to_add, ".jpeg"))
        type = jpeg;
    else if (strstr(path_to_add, ".jpg"))
        type = jpg;
    else if (strstr(path_to_add, ".png"))
        type = png;
    else if (strstr(path_to_add, ".html") || strstr(path_to_add, ".md"))
        type = html;
    else
        type = text;
    char header[MaxHeaderSize];
    memset(header, 0, MaxHeaderSize);
    generate_header(header, type, not_found);
    //std::cout << header;
    //std::cout << file_size;
    //std::cout << HeaderEnd;
    write(1, header, strlen(header));
    write(1, output_size, strlen(output_size));
    write(1, HeaderEnd,strlen(HeaderEnd));
    write(1, memory, file_size);
}

int serve_dinamic_content(int fd, char* full_path){
    const char* env_names[] = {"SERVER_PORT", "HOST_NAME", "STORAGE_DIRECTORY", "REQUEST_URI", "QUERY_STRING", NULL};
    size_t env_count = 1;
    while (env_names[env_count]) {
        env_count++;
    }

    char * new_environment[env_count];
    for (size_t i = 0; i < env_count; i++) {
        new_environment[i] = new char[MaxEnvironmentSize];
        memset(new_environment[i], 0, MaxEnvironmentSize);
    }
    new_environment[env_count] = NULL;

    const char* equal_sign = "=";
    for (size_t i = 0; i < env_count; i++) {
        strcat(new_environment[i], env_names[i]);
        strcat(new_environment[i], equal_sign);
        strcat(new_environment[i], getenv(env_names[i]));
    }
    char* newargv[] = {full_path, full_path, NULL};
    execve(newargv[0], newargv, new_environment);
}

int do_service(int fd) {

    if (strcmp(getenv("METHOD"), "GET") != 0) {
        std::cerr << "UNKNOWN METHOD" << std::endl;
        return -1;
    }
    if (strcmp(getenv("REQUEST_URI"), "/favicon.ico") == 0) {
        std::cerr << "favicon.ico ignored" << std::endl;
        //return 0;
    }
    char* storage_dir_path = getenv("STORAGE_DIRECTORY");
    char* path_to_add = getenv("REQUEST_URI");

    size_t size = strlen(storage_dir_path) + strlen(path_to_add);
    char full_path[size];
    memset(full_path, 0, size + 1);
    strcat(full_path, storage_dir_path);
    strcat(full_path, path_to_add);

    if (access(full_path, X_OK) == 0) {
        serve_dinamic_content(fd, full_path);
    } else if (access(full_path, R_OK) == 0) {
        serve_static_content(fd, false);
    } else {
        std::cerr << "CAN NOT OPEN FILE: " << full_path << std::endl;
        setenv("REQUEST_URI", "/404.txt", 1);
        serve_static_content(fd, true);
        return -1;
    }
    return 0;
}