#include <iostream>
#include "start_web_server.h"

const uint32_t default_port_number = 8000;
const char default_host_name[] = "127.0.0.1";

int main(int argc, char* argv[]) {
    if (argc < 2){
        std::cerr << "Storage directory is not specified" << std::endl;
        return -1;
    }
    setenv("STORAGE_DIRECTORY", argv[1], 1);

    uint32_t port_number = default_port_number;
    if (argc > 2) {
        port_number = strtoul(argv[2], NULL, 10);
    }

    char host_name[100];
    memset(host_name, 0, 100);
        strcpy(host_name, default_host_name);
    if (argc > 3) {
        strcpy(host_name, argv[3]);
    }


    setenv("HOST_NAME", host_name, 1);
    start_web_server(port_number, host_name);
}

